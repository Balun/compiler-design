import sys

class Parser(object):

    def __init__(self, tokens):
        self.tokens = tokens
        self.result = ""
        self.index = 0
        self.space = ""

    def parse(self):
        self.program()

    def get_result(self):
        return self.result

    def program(self):
        self.result += self.space + "<program>\n"
        self.space += " "
        self.lista_naredbi()
        self.space = self.space[:-1]

    def lista_naredbi(self):
        self.result += self.space + "<lista_naredbi>\n"
        self.space += " "

        if self.index >= len(self.tokens):
            self.result += self.space + "$\n"
            self.space = self.space[:-1]
            return

        if self.tokens[self.index].startswith("IDN") or self.tokens[self.index].startswith("KR_ZA"):
            self.naredba()
            self.lista_naredbi()

        elif  self.tokens[self.index].startswith("KR_AZ"):
            self.result += self.space + "$\n"

        else:
            self.print_error()

        self.space = self.space[:-1]

    def naredba(self):
        self.result += self.space + "<naredba>\n"
        self.space += " "

        if self.tokens[self.index].startswith("IDN"):
            self.naredba_pridruzivanja()

        elif self.tokens[self.index].startswith("KR_ZA"):
            self.za_petlja()

        else:
            self.print_error()

        self.space = self.space[:-1]

    def naredba_pridruzivanja(self):
        self.result += self.space + "<naredba_pridruzivanja>\n"
        self.space += " "
        self.result += self.space + self.tokens[self.index] + "\n"
        self.index+=1

        if self.tokens[self.index].startswith("OP_PRIDRUZI"):
            self.result += self.space + self.tokens[self.index] + "\n"
            self.index+=1
            self.expression()

        else:
            self.print_error()
            return

        self.space = self.space[:-1]

    def za_petlja(self):
        self.result += self.space + "<za_petlja>\n"
        self.space += " "

        self.result += self.space + self.tokens[self.index] + "\n"
        self.index += 1

        if self.tokens[self.index].startswith("IDN"):
            self.result += self.space + self.tokens[self.index] + "\n"
            self.index += 1
        else:
            self.print_error()
            return

        if self.tokens[self.index].startswith("KR_OD"):
            self.result += self.space + self.tokens[self.index] + "\n"
            self.index += 1
        else:
            self.print_error()
            return

        self.expression()

        if self.tokens[self.index].startswith("KR_DO"):
            self.result += self.space + self.tokens[self.index] + "\n"
            self.index += 1
        else:
            self.print_error()
            return

        self.expression()
        self.lista_naredbi()

        if self.tokens[self.index].startswith("KR_AZ"):
            self.result += self.space + self.tokens[self.index] + "\n"
            self.index += 1
        else:
            self.print_error()
            return

        self.space = self.space[:-1]

    def print_error(self):
        if self.index >= len(self.tokens):
            self.result = "err kraj\n"

        else:
            self.result = "err " + self.tokens[self.index] + "\n"

        sys.exit()

    def expression(self):
        self.result += self.space + "<E>\n"
        self.space += " "

        self.term()
        self.expression_list()

        self.space = self.space[:-1]

    def term(self):
        self.result += self.space +  "<T>\n"
        self.space += " "

        self.primary()
        self.term_list()

        self.space = self.space[:-1]

    def expression_list(self):
        self.result += self.space + "<E_lista>\n"
        self.space += " "

        if self.index >= len(self.tokens):
            self.result += self.space + "$\n"
            self.space = self.space[:-1]
            return

        if self.tokens[self.index].startswith("OP_PLUS"):
            self.result += self.space +  self.tokens[self.index] + "\n"
            self.index += 1
            self.expression()
        elif self.tokens[self.index].startswith("OP_MINUS"):
            self.result += self.space +  self.tokens[self.index] + "\n"
            self.index += 1
            self.expression()

        else:
            self.result += self.space + "$\n"

        self.space = self.space[:-1]

    def primary(self):
        self.result += self.space + "<P>\n"
        self.space += " "

        if self.index >= len(self.tokens):
            self.print_error()
            return

        if self.tokens[self.index].startswith("OP_PLUS"):
            self.result += self.space + self.tokens[self.index] + "\n"
            self.index += 1
            self.primary()

        elif self.tokens[self.index].startswith("OP_MINUS"):
            self.result += self.space + self.tokens[self.index] + "\n"
            self.index += 1
            self.primary()

        elif self.tokens[self.index].startswith("L_ZAGRADA"):
            self.result += self.space + self.tokens[self.index] + "\n"
            self.index += 1
            self.expression()

            if self.tokens[self.index].startswith("D_ZAGRADA"):
                self.result += self.space + self.tokens[self.index] + "\n"
                self.index += 1
            else:
                self.print_error()
                return

        elif self.tokens[self.index].startswith("IDN"):
            self.result += self.space + self.tokens[self.index] + "\n"
            self.index += 1

        elif self.tokens[self.index].startswith("BROJ"):
            self.result += self.space + self.tokens[self.index] + "\n"
            self.index += 1

        else:
            self.print_error()
            return

        self.space = self.space[:-1]

    def term_list(self):
        self.result += self.space + "<T_lista>\n"
        self.space += " "

        if self.index >= len(self.tokens):
            self.result += self.space + "$\n"
            self.space = self.space[:-1]
            return

        if self.tokens[self.index].startswith("OP_PUTA"):
            self.result += self.space + self.tokens[self.index] + "\n"
            self.index += 1
            self.term()
        elif self.tokens[self.index].startswith("OP_DIJELI"):
            self.result += self.space + self.tokens[self.index] + "\n"
            self.index += 1
            self.term()
        else:
            self.result += self.space + "$\n"

        self.space = self.space[:-1]

    def get_result(self):
        return self.result
