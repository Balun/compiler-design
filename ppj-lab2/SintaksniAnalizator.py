import sys
from parser import *


def main():
    lines = []

    for line in sys.stdin:
        line = line[:-1]
        lines.append(line)

    parser = Parser(lines)
    parser.parse()
    result = parser.get_result()


    print(result)

if __name__ == '__main__':
    main()