class Token:
    __symbol = None
    __line = None
    __content = None

    def __init__(self, symbol, line, content):
        self.__symbol = symbol
        self.__line = line
        self.__content = content

    def get_symbol(self):
        return self.__symbol

    def get_line(self):
        return self.__line

    def get_content(self):
        return self.__content

    def __str__(self):
        return self.__symbol + " " + str(self.__line) + " " + self.__content
