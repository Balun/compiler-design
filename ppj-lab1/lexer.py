import keys
from my_token import *


class Lexer:
    __text = None
    __index = None
    __line = None

    def __init__(self, text=""):
        self.__text = text
        self.__index = 0
        self.__line = 1

    def has_next(self):
        if self.__index < len(self.__text):
            return True

        return False

    def next_token(self):
        current = self.__text[self.__index]

        if current == '/':
            return self._check_slash_symbol()

        elif keys.contains_key(current):
            self.__index += 1
            return Token(keys.get_value(current), self.__line, current)

        elif current.isspace():
            self._consume_blanks()
            if self.has_next():
                return self.next_token()

            else:
                return Token("EOF", None, None)

        elif current.isdigit():
            return self._consume_number()

        elif current.isalpha():
            return self._consume_identifier()

        else:
            print("LEXER ERROR!!!")

    def _consume_blanks(self):
        current = self.__text[self.__index]

        while (current.isspace() and self.__index < len(self.__text)):
            if current == '\n':
                self.__line += 1

            self.__index += 1
            if self.__index < len(self.__text):
                current = self.__text[self.__index]
            else:
                break

    def _consume_comment(self):
        while (self.__text[self.__index] != '\n' and self.__index < len(self.__text)):
            current = self.__text[self.__index]
            self.__index += 1

        if (self.__text[self.__index] == '\n' and self.__index < len(self.__text)):
            current = self.__text[self.__index]
            self.__line += 1
            self.__index += 1

    def _consume_number(self):
        number = ""
        current = self.__text[self.__index]

        while (current.isdigit() and self.__index < len(self.__text)):
            number += current
            self.__index += 1
            current = self.__text[self.__index]

        return Token("BROJ", self.__line, number)

    def _consume_identifier(self):
        current = self.__text[self.__index]
        identifier = ""

        while ((current.isalpha() or current.isdigit()) and (self.__index < len(self.__text))):
            identifier += current
            self.__index += 1
            current = self.__text[self.__index]

        if keys.contains_key(identifier):
            return Token(keys.get_value(identifier), self.__line, identifier)
        else:
            return Token("IDN", self.__line, identifier)

    def _check_slash_symbol(self):
        try:
            current = self.__text[self.__index + 1]
            if (current == '/'):
                self._consume_comment()
                if self.has_next():
                    return self.next_token()

                else:
                    return Token("EOF", None, None)

            else:
                self.__index += 1
                return Token(keys.get_value("/"), self.__line, "/")
        except IndexError:
            self.__index += 1
            return Token("EOF", None, None)
