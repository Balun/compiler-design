from lexer import *

def main():
    source_path = "/media/lumba/OS/matej/FER/5. semestar/prevođenje progamskih jezika/labosi/PPJ" + \
                  "_Labosi_jednostavna_inacica_-_testni_primjeri_(nagadam_da_ce_biti_isti_i_ove_godine)/jedlab1_primjeri/30_retci"

    source = open(source_path + "/test.in", "r")
    text = source.read()

    dest = open(source_path + "/test.out", "r")
    result = dest.read()

    tokens = ""

    lexer = Lexer(text)

    while lexer.has_next():
        token = lexer.next_token()
        if (token.get_symbol() == "EOF"):
            break

        tokens = tokens + token.__str__() + "\n"

    if (tokens == result):
        print("Test uspješan")
    else:
        print("Test nije uspješan")

    source.close()
    dest.close()


if __name__ == '__main__':
    main()
