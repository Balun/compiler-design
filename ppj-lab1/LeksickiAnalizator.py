import sys
from lexer import *

def main():
    text = ""

    for line in sys.stdin:
        text = text + line;

    tokens = []

    lexer = Lexer(text)

    while lexer.has_next():
        token = lexer.next_token()
        if (token.get_symbol() == "EOF"):
            break

        tokens.append(token)

    for line in tokens:
        print(line.__str__())


if __name__ == '__main__':
    main()
