keys = {"=": "OP_PRIDRUZI", "+": "OP_PLUS", "-": "OP_MINUS", "*": "OP_PUTA", "/": "OP_DIJELI",
        "(": "L_ZAGRADA", ")": "D_ZAGRADA", "za": "KR_ZA", "od": "KR_OD", "do": "KR_DO", "az": "KR_AZ"}


def contains_key(key):
    if keys.__contains__(key):
        return True
    
    return False


def get_value(key):
    return keys[key]
