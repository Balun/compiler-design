import sys

class Semantic_analyzer(object):
    def __init__(self, input_lines):
        self.lines = input_lines
        self.loop = 0
        self.variables = {}
        self.result = ""
        self.index = 0
        self.nested = {}
        self.loop_stack = []
        self.tmp = None
        self.analise()

    def analise(self):
        if self.lines[self.index].startswith("<program>"):
            self.index += 1
            self.program()

    def program(self):
        if self.lines[self.index].startswith("<lista_naredbi>"):
            self.index += 1
            self.lista_naredbi()

    def lista_naredbi(self):
        if self.index >= len(self.lines):
            return

        if self.lines[self.index].startswith("<naredba>"):
            self.index += 1
            self.naredba()
            self.index += 1
            self.lista_naredbi()

        else:
            self.index += 1

    def naredba(self):
        if self.lines[self.index].startswith("<naredba_pridruzivanja>"):
            self.index += 1
            self.naredba_pridruzivanja()

        else:
            self.index += 1
            self.za_petlja()

    def naredba_pridruzivanja(self):
        name = self.lines[self.index].split(" ")[2]

        if name not in self.variables:
            self.variables[name] = int(self.lines[self.index].split(" ")[1])
            self.tmp = name

            if self.loop > 0:
                self.nested[self.loop] = name

        self.index += 2
        self.element()
        self.tmp = None

    def za_petlja(self):
        self.loop += 1
        self.index += 1
        name = self.lines[self.index].split(" ")[2]
        line = self.lines[self.index].split(" ")[1]
        pom = None
        if name in self.variables:
            pom = self.variables[name]

        self.variables[name] = line
        self.loop_stack.append(name)

        self.index += 2
        self.element()
        self.index += 1
        self.element()

        if self.loop_stack:
            self.loop_stack.pop()

        self.index += 1
        self.lista_naredbi()
        self.index += 1

        if pom is None:
            self.variables.__delitem__(name)

        else:
            self.variables[name] = pom

        if self.loop in self.nested:
            for var in self.nested[self.loop]:
                if var in self.variables:
                    self.variables.__delitem__(var)

        self.loop -= 1

    def element(self):
        self.index += 1
        self.term()
        self.index += 1
        self.element_list()

    def element_list(self):
        if self.lines[self.index].startswith("OP_PLUS") or \
                self.lines[self.index].startswith("OP_MINUS"):
            self.index += 1
            self.element()

        else:
            self.index += 1

    def term(self):
        self.index += 1
        self.primary()
        self.index += 1
        self.term_list()

    def term_list(self):
        self.index +=1
        if self.lines[self.index].startswith("OP_PUTA") or \
                self.lines[self.index].startswith("OP_DIJELI"):
            self.index += 1
            self.term()

        else:
            self.index += 1

    def primary(self):
        self.index +=1
        if self.lines[self.index].startswith("OP_PLUS") or \
                self.lines[self.index].startswith("OP_MINUS"):
            self.index += 1
            self.primary()

        elif self.lines[self.index].startswith("L_ZAGRADA"):
            self.index += 1
            self.element()

        elif self.lines[self.index].startswith("IDN"):
            name = self.lines[self.index].split(" ")[2]
            line = self.lines[self.index].split(" ")[1]

            if name not in self.variables:
                self.print_error(line, name)

            elif self.loop_stack and name == self.loop_stack[len(self.loop_stack) - 1]:
                self.loop_stack.append(name)
                self.print_error(line, name)

            elif self.tmp == name:
                self.print_error(line, name)

            else:
                self.result += line + " " + str(self.variables[name]) + " " + name + "\n"

    def print_error(self, line, name):
        print(self.result, end="")
        print("err" + " " + line + " " + name)
        sys.exit(-1)

    def get_result(self):
        return self.result
