from semantic import *

def main():
    lines = []

    for line in sys.stdin:
        line = line[:-1]
        line = line.strip()
        lines.append(line)

    semantic = Semantic_analyzer(lines)
    result = semantic.get_result()


    print(result)

if __name__ == '__main__':
    main()