from semantic import *

def main():

    path = "/media/lumba/OS/matej/FER/5. semestar/prevođenje progamskih jezika/labosi/testovi/" \
           "jedlab3_primjeri/15_gen/"

    lines = [line.rstrip('\n') for line in open(path + "test.in")]
    lines = [line.strip() for line in lines]
    semantic = Semantic_analyzer(lines)

    result = semantic.get_result()

    with open(path + "test.out", 'r') as myfile:
        correct = myfile.read()

    if result == correct:
        print("Test uspješan")
    else:
        print("Test nije uspješan")

    myfile.close()

    '''
    f = open("/media/lumba/OS/matej/FER/5. semestar/prevođenje progamskih jezika/labosi/2/pokusaj.out", 'w')
    f.write(result)  # python will convert \n to os.linesep
    f.close()  # you can omit in most cases as the destructor will call it
    '''


if __name__ == '__main__':
    main()